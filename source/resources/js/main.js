// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {
  
  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 
      
    },
    finalize: function() {
      
    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 
      
    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here
 
    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;
 
    // hit up common first.
    UTIL.fire( 'common' );
 
    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);

/* ================================
                  ISOTOPE
=================================== */

var rowHeight = $('.work > .row-fluid').outerHeight();
var footerHeight = $('.footer').outerHeight();
var graphicHeight = $('.approach .main-content .content').outerHeight();
var blogHeight = $('.blogBlock ul').outerHeight();

$(document).ready(function(){
  if( $(window).width() > 940)
{
  $('.featured').css('height', rowHeight);
  $('.secondary .row-fluid div[class*="col"]').css('height', rowHeight / 2);
} else {
  $('.featured, .secondary .row-fluid div[class*="col"]').css('height', 'auto');
}
});

$(window).resize(function(){
  if( $(window).width() > 940)
{
  $('.featured').css('height', rowHeight);
  $('.secondary .row-fluid div[class*="col"]').css('height', rowHeight / 2);
} else {
  $('.featured, .secondary .row-fluid div[class*="col"]').css('height', 'auto');

}

});


$(document).ready(function(){
  if( $(window).width() > 940)
{
  $('.workWith, .subscribe').css('height', footerHeight);
} else {
  $('.workWith, .subscribe').css('height', 'auto');
}
});

$(window).resize(function(){
  if( $(window).width() > 940)
{
  $('.workWith, .subscribe').css('height', footerHeight);
} else {
  $('.workWith, .subscribe').css('height', 'auto');

}

});


/*$(document).ready(function(){
  if( $(window).width() > 940)
{
  $('.blogBlock ul li').css('height', blogHeight);
} else {
  $('.blogBlock ul li').css('height', 'auto');
}
});

$(window).resize(function(){
  if( $(window).width() > 940)
{
  $('.blogBlock ul li').css('height', blogHeight);
} else {
  $('.blogBlock ul li').css('height', 'auto');

}

});*/


$(document).ready(function(){

    

  if( $(window).width() > 767)
{
  $('.approach .main-content .graphic').css('height', graphicHeight);
} else {
  $('.approach .main-content .graphic').css('height', 'auto');
}
});

$(window).resize(function(){
  if( $(window).width() > 767)
{
  $('.approach .main-content .graphic').css('height', graphicHeight);
} else {
  $('.approach .main-content .graphic').css('height', 'auto');

}

});

$('.filters li').click(function(e){
  e.preventDefault;
  $('.filters li').removeClass('active');
  $(this).addClass('active');
});



( function( $, window, document, undefined )
{
    'use strict';
 
    var $list       = $( '.blogBlock ul' ),
        $items      = $list.find( 'li' ),
        setHeights  = function()
        {
            $items.css( 'height', 'auto' );
 
            var perRow = Math.floor( $list.width() / $items.width() );
            if( perRow == null || perRow < 2 ) return true;
 
            for( var i = 0, j = $items.length; i < j; i += perRow )
            {
                var maxHeight   = 0,
                    $row        = $items.slice( i, i + perRow );
 
                $row.each( function()
                {
                    var itemHeight = parseInt( $( this ).outerHeight() );
                    if ( itemHeight > maxHeight ) maxHeight = itemHeight;
                });
                $row.css( 'height', maxHeight );
            }
        };
 
    setHeights();
    $( window ).on( 'resize', setHeights );
 
})( jQuery, window, document );


/*$(function() {
    var tags = $('.meta').text();
      var tagArray = tags.split('/').slice(0, 2);
      var taglist = '';

      $.each(tagArray, function(key, val){
        taglist += $.trim(val);

        $('.meta').append($.trim(val));

        if (key < 1)
        {
          taglist += ' / ';
        }

        console.log(taglist);
      });

      $('.meta').text(taglist);
  });*/


$(function(){
    $('.item').each(function() {
      $(this).find('.meta').each(function(key, val){
        var text = $(this).text(),
            str = text.split('/').slice(0,2);


        $(this).text(str);
        
        console.log(text);
      });
  });
});



