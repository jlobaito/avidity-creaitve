<?php
/**
 * @package contactsubmissions
 */
$contact = $modx->getService('contactsubmissions','ContactSubmissions',$modx->getOption('contactsubmissions.core_path',null,$modx->getOption('core_path').'components/contactsubmissions/').'model/contactsubmissions/',$scriptProperties);
if (!($contact instanceof ContactSubmissions)) return '';

/* setup default properties */
$tpl = $modx->getOption('tpl',$scriptProperties,'rowTpl');
$sort = $modx->getOption('sort',$scriptProperties,'name');
$dir = $modx->getOption('dir',$scriptProperties,'ASC');

/* build query */
$c = $modx->newQuery('ContactSubmission');
$c->sortby($sort,$dir);
$contactsubmissions = $modx->getCollection('ContactSubmission',$c);

/* iterate */
$output = '';
foreach ($contactsubmissions as $contactsubmission) {
    $contactsubmissionArray = $contactsubmission->toArray();
    $output .= $contact->getChunk($tpl,$contactsubmissionArray);
}

return $output;